# 开源教育培训排课软件OpenPDBPS

#### 介绍
开源图片文档批量处理软件（Open Pictrue Document Batch Process Soft）简称为OpenPDBPS，制作本软件目的是为了批量处理A3幅面的PDF格式的试卷分割为A4幅面打印。随着功能的增加，PDF转JPG图片、批量分割图片、批量顺序打印图片等均可本机使用，无需上传网络处理。


#### 软件架构
OpenPDBPS为Windows单机软件，使用.net 4.X（C#/WPF）技术开发，以开源方式（木兰公共许可证第2版，MulanPubL-2.0）发布本软件。


#### 安装教程
发行版安装包网盘链接: https://pan.baidu.com/s/1DyRkmr0MPy-jRqU7zuGaig 提取码: iuen ，双击开源图片文档批量处理软件.msi安装后即可在Windows桌面启用本软件。


#### 使用说明
详情请看软件帮助。


#### 参与贡献
1. 所有者：dinid(来自littleMIS)


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
