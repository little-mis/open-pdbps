﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using LiteMain;

namespace OpenPDBPS
{
    /// <summary>
    /// MainPDBPS.xaml 的交互逻辑
    /// </summary>
    public partial class MainPDBPS : Window
    {
        List<ObjFile> list;//定义文件对象集合

        public MainPDBPS()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            labelLoginName.Content = "软件版本：" + DalDataConfig.SoftVerion;
        }

        private void MiExit_Click(object sender, RoutedEventArgs e)
        {
            BaseWindowClass.AppExit();
        }

        private void MiHelpFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("help.doc");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void MiAboutBox_Click(object sender, RoutedEventArgs e)
        {
            new WindowAbout().ShowDialog();
        }

        private void MiConsult_Click(object sender, RoutedEventArgs e)
        {
            new WindowConsult().ShowDialog();
        }

        private void BtnGetPicDir_Click(object sender, RoutedEventArgs e)
        {
            list= DalFile.GetDirFiles(BaseFileClass.ExtJpg);
            LoadDataGrid();
        }

        private void BtnGetPics_Click(object sender, RoutedEventArgs e)
        {
            list = DalFile.GetSelectFiles(BaseWindowClass.JpgFile);
            LoadDataGrid();
        }

        private void LoadDataGrid()
        {
            dataGrid1.ItemsSource = list;
            dataGrid1.LoadingRow += new EventHandler<DataGridRowEventArgs>(BaseWindowClass.DataGrid_LoadingRow);//显示行号  
        }

        private void BtnLeftRight_Click(object sender, RoutedEventArgs e)
        {
            if (BaseListClass.CheckNull(list) == false)
            {
                List<ObjFile> files = new List<ObjFile>();
                foreach (ObjFile i in list)
                {
                    try
                    {
                        Bitmap source = new Bitmap(i.FilePath);
                        //左侧
                        ObjRect left = new ObjRect { XStart = 1, XLength = source.Width / 2 - 1, YStart = 1, YLength = source.Height - 1, RectCode = "l" };
                        string path = BaseFileClass.GetAppendFileName(i.FilePath, "_l");
                        DalFile.BitmapSave(source, left, path);
                        files.Add(new ObjFile { FileName = BaseFileClass.GetFileName(path,false),FilePath= path });
                        //右侧
                        path = BaseFileClass.GetAppendFileName(i.FilePath, "_r");
                        ObjRect right = new ObjRect { XStart = source.Width / 2 + 1, XLength = source.Width / 2 - 1, YStart = 1, YLength = source.Height - 1, RectCode = "r" };
                        DalFile.BitmapSave(source, right, path); 
                        files.Add(new ObjFile { FileName = BaseFileClass.GetFileName(path, false), FilePath = path });
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
                if(BaseListClass.CheckNull(files)==false)//UI显示
                {
                    list = files;
                    dataGrid1.ItemsSource = null;
                    LoadDataGrid();
                }
            }
            else
            {
                MessageBox.Show("没有需要处理的图片，请先选择图片！");
            }
        }

        private void BtnPrintAll_Click(object sender, RoutedEventArgs e)
        {
            if (BaseListClass.CheckNull(list) == false)
            {
                BasePrintImageClass print = new BasePrintImageClass();
                foreach (ObjFile i in list)
                {
                    print.f = i;
                    print.PrintPreview();
                }
            }
        }

        private void BtnPrintOddNumber_Click(object sender, RoutedEventArgs e)
        {
            if (BaseListClass.CheckNull(list) == false)
            {
                BasePrintImageClass print = new BasePrintImageClass();
                for(int i =0;i<list.Count;i++)
                {
                    if (i < list.Count)
                    {
                        print.f = list[i];
                        print.PrintPreview();
                        i++;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        private void BtnPrintEvenNumber_Click(object sender, RoutedEventArgs e)
        {
            if (BaseListClass.CheckNull(list) == false)
            {
                BasePrintImageClass print = new BasePrintImageClass();
                for (int i = 1; i < list.Count; i++)
                {
                    if (i < list.Count)
                    {
                        print.f = list[i];
                        print.PrintPreview();
                        i++;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        private void BtnGetPdfDir_Click(object sender, RoutedEventArgs e)
        {
            list = DalFile.GetDirFiles(BaseFileClass.ExtPdf);
            LoadDataGrid();
        }


        private void BtnGetPdfs_Click(object sender, RoutedEventArgs e)
        {
            list = DalFile.GetSelectFiles(BaseWindowClass.PdfFile);
            LoadDataGrid();
        }

        private void BtnPdf2Pic_Click(object sender, RoutedEventArgs e)
        {
            if(BaseListClass.CheckNull(list)==false)
            {
                List<ObjFile> l = new List<ObjFile>();
                foreach (ObjFile i in list)
                {
                    DalPdf.Pdfium2Image(i.FilePath,ref l);
                }
                if(BaseListClass.CheckNull(l)==false)
                {
                    list = l;
                    LoadDataGrid();
                }
            }
        }

        private void dataGrid1_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (dataGrid1.SelectedItem != null)
            {
                ObjFile obj = dataGrid1.SelectedItem as ObjFile;
                System.Diagnostics.Process.Start(obj.FilePath);
            }
        }

        private void dataGrid1_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if(e.Key==System.Windows.Input.Key.Delete)
            {
                int row = dataGrid1.SelectedIndex;
                if(row>=0 && BaseListClass.CheckNull(list)==false)
                {                    
                    list.RemoveAt(row);
                    dataGrid1.ItemsSource = null;
                    LoadDataGrid();
                }
            }
        }
    }
}
