﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace LiteMain
{
    /// <summary>
    /// 窗体操作类
    /// </summary>
    public class BaseWindowClass
    {
        public const string JpgFile = "图片文件(*.jpg)|*.jpg";
        public const string PdfFile = "PDF文件(*.pdf)|*.pdf";

        /// <summary>
        /// 退出当前应用程序
        /// 2022-03-17
        /// </summary>
        public static void AppExit()
        {
            if (System.Windows.MessageBox.Show("确定退出当前程序吗?", "退出", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.Yes) == MessageBoxResult.Yes)
            {
                System.Environment.Exit(0);
            }
        }

        /// <summary>
        /// DataGrid行头自动增加行号
        /// </summary>
        public static void DataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = e.Row.GetIndex() + 1;
        }

        /// <summary>
        /// 返回选择的多个文件
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="IsMultiselect"></param>
        /// <returns></returns>
        public static string[] GetSelectFiles(string filter)
        {
            string[] result = null;
            Microsoft.Win32.OpenFileDialog fileDialog1 = new Microsoft.Win32.OpenFileDialog();
            //fileDialog1.InitialDirectory =Directory.GetCurrentDirectory();//初始目录
            fileDialog1.Filter = filter;//过滤文件的类型
            fileDialog1.Multiselect = true;//多选文件
            fileDialog1.FilterIndex = 1;
            fileDialog1.RestoreDirectory = false;//返回目录
            if (fileDialog1.ShowDialog() == true)
            {
                result = fileDialog1.FileNames;
            }
            return result;
        }

        /// <summary>
        /// 返回选择的路径
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="IsMultiselect"></param>
        /// <returns></returns>
        public static string GetSelectPath()
        {
            string result = string.Empty;
            System.Windows.Forms.FolderBrowserDialog fileDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            fileDialog1.ShowDialog();
            if (fileDialog1.SelectedPath != string.Empty)
            {
                result = fileDialog1.SelectedPath;
            }
            return result;
        }
    }
}
