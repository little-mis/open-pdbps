﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace LiteMain
{
    /// <summary>
    /// DataTable操作类
    /// </summary>
    public class BaseDataTable
    {
        /// <summary>
        /// 判断DataTable是否空（无数据行）
        /// </summary>
        /// <param name="dt">DataTable</param>
        /// <returns>bool</returns>
        public static bool CheckNull(DataTable dt)
        {
            bool result = true;
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    dt.AcceptChanges();
                    result = false;
                }
            }
            return result;
        }

        /// <summary>
        /// 检查DataTable中指定列名是否存在
        /// </summary>
        /// <param name="columnName">列名字符串</param>
        /// <param name="dt">DataTable</param>
        /// <returns>bool</returns>
        private static bool CheckColumnName(string columnName, DataTable dt)
        {
            bool result = false;
            if (CheckNull(dt) == false)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (columnName == dt.Columns[i].ColumnName)
                    {
                        result = true;
                        break;
                    }
                }
            }
            return result;
        }
    }
}
