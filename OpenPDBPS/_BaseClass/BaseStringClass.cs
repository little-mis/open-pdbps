﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;


namespace LiteMain
{
    public class BaseStringClass
    {
        /// <summary>
        ///  数字转指定长度字符串
        /// </summary>
        /// <param name="num">数字</param>
        /// <param name="length">字符串长度</param>
        /// <returns></returns>
        public static string NumberToLengthString(int num, int length)
        {
            if (num.ToString().Length < length)
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < length - num.ToString().Length; i++)
                {
                    sb.Append("0");
                }
                return sb.Append(num).ToString();
            }
            else
            {
                return num.ToString();
            }
        }
    }
}
