﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Drawing;

namespace OpenPDBPS
{
    public class BasePrintImageClass
    {
        private int printNum = 0;//多页打印
        public ObjFile f = null;//单个图片文件
        public   List<ObjFile> fileList= new List<ObjFile>();//多个图片文件
        
        public void PrintPreview()
        {
            PrintDocument docToPrint = new PrintDocument();
            docToPrint.BeginPrint += new System.Drawing.Printing.PrintEventHandler(this.docToPrint_BeginPrint);
            docToPrint.EndPrint += new System.Drawing.Printing.PrintEventHandler(this.docToPrint_EndPrint);
            docToPrint.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.docToPrint_PrintPage);
            docToPrint.DefaultPageSettings.Landscape = false;
            docToPrint.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
            System.Windows.Forms.PrintDialog printDialog = new System.Windows.Forms.PrintDialog();
            //printDialog.AllowSomePages = true;
            //printDialog.ShowHelp = true;
            //printDialog.Document = docToPrint;

            //if (printDialog.ShowDialog() == DialogResult.OK)
            //{
                docToPrint.Print();
            //}
        }

        private void docToPrint_BeginPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (fileList.Count == 0)
                fileList.Add(f);
        }
        private void docToPrint_EndPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }
        private void docToPrint_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            //图片抗锯齿
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            Stream fs = new FileStream(f.FilePath, FileMode.Open, FileAccess.Read);           
            System.Drawing.Image img = System.Drawing.Image.FromStream(fs);          
            int x = e.MarginBounds.X;
            int y = e.MarginBounds.Y;
            int width = img.Width;
            int height = img.Height;
            //超过A4宽高比则顺时针旋转90°
            if (width / height > 2100/2970)
            {
                img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                width = img.Width;
                height = img.Height;
            }

            if ((width / e.MarginBounds.Width) > (height / e.MarginBounds.Height))
            {
                width = e.MarginBounds.Width;
                height = img.Height * e.MarginBounds.Width / img.Width;
            }
            else
            {
                height = e.MarginBounds.Height;
                width = img.Width * e.MarginBounds.Height / img.Height;
            }

            e.Graphics.DrawImage(img, e.MarginBounds);
            string txt = string.Empty;
            if (f.IsPrint==true)
            {
                txt = txt+f.FilePath;
            }
            if(f.IsPrint==true)
            {
                txt = txt +"，" +f.FileName;
            }
            if (printNum < fileList.Count - 1)
            {
                printNum++;
                e.HasMorePages = true;
                return;
            }
            e.HasMorePages = false;
        }
    }
}