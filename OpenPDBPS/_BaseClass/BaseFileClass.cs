﻿using System;
using System.IO;

namespace LiteMain
{
    /// <summary>
    /// 文件夹操作类
    /// </summary>
    public abstract class BaseFileClass
    {
        public static string ExtJpg = "*.jpg";
        public static string ExtPdf = "*.pdf";

        /// <summary>
        /// 根据传来的文件全路径,获取文件名称部分
        /// </summary>
        /// <param name="FileFullPath">文件全路径</param>
        /// <param name="IncludeExtension">是否包括文件扩展名</param>
        /// <returns>string 文件名称</returns>
        public static string GetFileName(string FileFullPath, bool IncludeExtension)
        {
            if (File.Exists(FileFullPath) == true)
            {
                FileInfo F = new FileInfo(FileFullPath);
                if (IncludeExtension == true)
                {
                    return F.Name;
                }
                else
                {
                    return F.Name.Replace(F.Extension, "");
                }
            }
            else//文件不存在
            {
                return null;
            }
        }


        /// <summary>
        /// 返回文件名+字符串+扩展名
        /// </summary>
        /// <param name="FileFullPath"></param>
        /// <param name="AppendName"></param>
        /// <returns></returns>
        public static string GetAppendFileName(string FileFullPath, string AppendName, bool AddDateTime = false)
        {
            if (File.Exists(FileFullPath) == true)
            {
                FileInfo fi = new FileInfo(FileFullPath);
                if (AddDateTime == true)
                {
                    return fi.DirectoryName + "\\" + fi.Name.Replace(fi.Extension, "") + AppendName + DateTime.Now.ToString("yyyyMMhh_HHmmss") + fi.Extension;
                }
                else
                {
                    return fi.DirectoryName + "\\" + fi.Name.Replace(fi.Extension, "") + AppendName + fi.Extension;
                }
            }
            else//文件不存在
            {
                return null;
            }
        }
    }
}