﻿namespace OpenPDBPS
{
    //基础设置数据操作类
    public class DalDataConfig
    {
        //程序名
        public static string SoftName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Namespace;
        //版本
        public const string SoftVerion = "1.1.0";

    }
}
