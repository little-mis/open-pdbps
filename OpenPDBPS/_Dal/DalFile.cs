﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using LiteMain;

namespace OpenPDBPS
{
    /// <summary>
    /// 文件对象操作类
    /// 创建人：dinid
    /// 创建日期：20220318
    /// 版本：1.0
    /// </summary>
    public class DalFile
    {
        /// <summary>
        /// Bitmap指定区域保存为文件
        /// 2022-03-18
        /// </summary>
        /// <param name="source">Bitmap</param>
        /// <param name="r">ObjRect</param>
        /// <param name="path">FileFullPath</param>
        /// <returns></returns>
        public static bool BitmapSave(Bitmap source, ObjRect r, string fileFullPath)
        {
            bool result = false;
            //定义目标
            try
            {
                System.Drawing.Rectangle destRect = new System.Drawing.Rectangle(0, 0, r.XLength, r.YLength);//目标区域
                Bitmap destBitmap = new Bitmap(r.XLength, r.YLength);//目标位图
                Graphics g = Graphics.FromImage(destBitmap);//目标位图创建GDI+绘图图面
                System.Drawing.Rectangle sourceRect = new System.Drawing.Rectangle(r.XStart, r.YStart, r.XLength, r.YLength);//定义原图区域
                g.DrawImage(source, destRect, sourceRect, GraphicsUnit.Pixel);//从原图区域获取内容绘制到目标区域，进而g获得图片内容
                //string destPath = BaseFileClass.GetAppendName(r.RectCode, fileFullPath);
                //destBitmap.RotateFlip(RotateFlipType.Rotate180FlipNone);//图像旋转180°
                destBitmap.Save(fileFullPath, System.Drawing.Imaging.ImageFormat.Jpeg); //保存图片为指定格式
                g.Dispose();//释放绘图资源
                destBitmap.Dispose();//释放目标位图
                result = true;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 返回文件夹下指定查询条件的文件集合
        /// 2022-03-18
        /// </summary>
        /// <param name="searchPattern"></param>
        /// <returns></returns>
        static public List<ObjFile> GetDirFiles(string searchPattern)
        {
            List<ObjFile> result = new List<ObjFile>();
            string[] fileNames = BaseDirClass.GetDirFiles(BaseWindowClass.GetSelectPath(), searchPattern);
            if (fileNames != null && fileNames.Length>0)
            {
                for (int i = 0; i < fileNames.Length; i++)
                {
                    FileInfo fi = new FileInfo(fileNames[i]);
                    result.Add(new ObjFile { FileID = i, FileName = fi.Name, FilePath = fi.FullName });
                }
            }
            return result;
        }

        /// <summary>
        /// 返回文件夹下指定查询条件的文件集合
        /// 2022-03-19
        /// </summary>
        /// <param name="searchPattern"></param>
        /// <returns></returns>
        static public List<ObjFile> GetSelectFiles(string filter)
        {
            List<ObjFile> result = new List<ObjFile>();
            string[] fileNames = BaseWindowClass.GetSelectFiles(filter);
            if (fileNames != null && fileNames.Length > 0)
            {
                for (int i = 0; i < fileNames.Length; i++)
                {
                    FileInfo fi = new FileInfo(fileNames[i]);
                    result.Add(new ObjFile { FileID = i, FileName = fi.Name, FilePath = fi.FullName });
                }
            }
            return result;
        }
    }
}
