﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing.Imaging;
using System.Windows.Controls;
using LiteMain;
using PdfiumViewer;

namespace OpenPDBPS
{
    /// <summary>
    /// PDF文件操作类  
    /// 创建人：dinid
    /// 创建日期：20220318
    /// 版本：1.0
    /// </summary>
    public class DalPdf
    {
        /// <summary>
        /// 批量PDF文件转图片
        /// 创建人：dinid
        /// 创建日期：20220318
        /// 版本：1.0
        /// </summary>
        /// <param name="path"></param>
        /// <param name="pdfFiles"></param>
        /// <returns></returns>
        public static bool Pdfium2Image(string path, ref List<ObjFile> pdfFiles)
        {
            bool result = false;
            if (File.Exists(path) == true)
            {
                FileInfo fi = new FileInfo(path);
                if (fi.Extension != "pdf")
                {
                    try
                    {
                        var pdf = PdfiumViewer.PdfDocument.Load(path);
                        var pdfPages = pdf.PageCount;
                        var pdfSizes = pdf.PageSizes;
                        for (int i = 0; i < pdfPages; i++)
                        {
                            System.Drawing.Size size = new System.Drawing.Size();
                            size.Height = (int)pdfSizes[(i)].Height * 2;
                            size.Width = (int)pdfSizes[(i)].Width * 2;
                            string pdfPath =fi.DirectoryName+ "\\" + fi.Name.Replace(fi.Extension, "") + "_"+ BaseStringClass.NumberToLengthString(i + 1, 4)+".jpg";
                            RenderPage(path, i, size, pdfPath);
                            pdfFiles.Add(new ObjFile { FileName = BaseFileClass.GetFileName(pdfPath, false), FilePath = pdfPath });
                        }
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        result = false;
                    }
                }
                else
                {
                    System.Windows.MessageBox.Show(fi.Name+"不是PDF文件！");
                }
            }
            else
            {
                System.Windows.MessageBox.Show(path + "不存在！");
            }
            return result;
        }

        static void RenderPage(string pdfPath, int pageNumber, System.Drawing.Size size, string outputpath, int dpi = 300)
        {
            using (var document = PdfiumViewer.PdfDocument.Load(pdfPath))
            {
                using (var stream = new FileStream(outputpath, FileMode.Create))
                {
                    using (var image = GetPageImage(pageNumber, size, document, dpi))
                    {
                        image.Save(stream, ImageFormat.Jpeg);
                    }
                }
            }
        }

        static System.Drawing.Image GetPageImage(int pageNumber, System.Drawing.Size size, PdfiumViewer.PdfDocument document, int dpi)
        {
            return document.Render(pageNumber, size.Width, size.Height, dpi, dpi, PdfRenderFlags.Annotations);
        }
    }
}
