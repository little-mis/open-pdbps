﻿using System;

namespace OpenPDBPS
{
    /// <summary>
    /// 图形区域类 
    /// 创建人：dinid
    /// 创建日期：20220318
    /// 版本：1.0
    /// </summary>
    public class ObjRect
    {
        public int RectID { get; set; }
        public Guid RectGUID { get; set; }
        public string RectCode { get; set; }
        public string RectName { get; set; }
        public int XStart { get; set; }
        public int XLength { get; set; }
        public int YStart { get; set; }
        public int YLength { get; set; }
        public string Remark { get; set; }
        public bool DeleteMark { get; set; }
        public DateTime Uptime { get; set; }
    }
}
