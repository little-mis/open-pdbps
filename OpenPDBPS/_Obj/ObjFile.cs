﻿namespace OpenPDBPS
{
    public class ObjFile
    {
        /// <summary>  
        /// 图形对象类
        /// 创建人：dinid
        /// 创建日期：20220318
        /// 版本：1.0
        /// </summary>
        public int FileID { get; set; }//图片存储方式
        public string FileName { get; set; }//图片名
        public string FilePath { get; set; }//图片文件路径
        public byte[] FileByte { get; set; }//图片文件二进制流
        public bool IsPrint { get; set; }
    }
}

